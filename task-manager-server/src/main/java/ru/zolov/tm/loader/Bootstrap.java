package ru.zolov.tm.loader;

import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.endpoint.AbstractEndpoint;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.service.DomainService;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.SessionService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;
import ru.zolov.tm.service.UserService;
import ru.zolov.tm.util.PublisherUtil;
import ru.zolov.tm.util.SqlSessionFactoryUtil;

public final class Bootstrap implements ServiceLocator {

  private final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryUtil.getSqlSessionFactory();
  private final IProjectService projectService = new ProjectService(this);
  private final ITaskService taskService = new TaskService(this);
  private final IUserService userService = new UserService(this);
  private final IDomainService domainService = new DomainService(this);
  private final ISessionService sessionService = new SessionService(this);
  private final TerminalService terminalService = new TerminalService();
  private final Set<AbstractEndpoint> endpoits = new LinkedHashSet<>();
  private final Set<Class<? extends AbstractEndpoint>> endpointClasses = new Reflections("ru.zolov.tm.endpoint")
      .getSubTypesOf(AbstractEndpoint.class);

  @Override public @NotNull SqlSessionFactory getSqlSessionFactory() {
    return sqlSessionFactory;
  }

  @NotNull @Override public IProjectService getProjectService() {
    return projectService;
  }

  @NotNull @Override public ITaskService getTaskService() {
    return taskService;
  }

  @NotNull @Override public TerminalService getTerminalService() {
    return terminalService;
  }

  @NotNull @Override public IUserService getUserService() {
    return userService;
  }

  @NotNull @Override public IDomainService getDomainService() {
    return domainService;
  }

  @NotNull @Override public ISessionService getSessionService() {
    return sessionService;
  }

  public void registryEndpoint(
      @NotNull final Set<Class<? extends AbstractEndpoint>> classes
  ) throws Exception {
    for (@NotNull final Class clazz : classes) {
      registryEndpoint(clazz);
    }
  }

  public void registryEndpoint(
      @NotNull final Class clazz
  ) throws Exception {
    if (!AbstractEndpoint.class.isAssignableFrom(clazz)) return;
    Object endpoint = clazz.getDeclaredConstructor().newInstance();
    AbstractEndpoint abstractEndpoint = (AbstractEndpoint)endpoint;
    registryEndoint(abstractEndpoint);
  }

  public void registryEndoint(AbstractEndpoint endpoint) throws EmptyRepositoryException {
    if (endpoint == null) throw new EmptyRepositoryException();
    endpoint.setServiceLocator(this);
    endpoits.add(endpoint);
  }

  public void init() {
    try {
      BasicConfigurator.configure();
      registryEndpoint(endpointClasses);
      PublisherUtil.publish(endpoits);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
  }
}