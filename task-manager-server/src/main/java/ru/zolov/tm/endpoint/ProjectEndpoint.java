package ru.zolov.tm.endpoint;

import java.util.Comparator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectEndpoint;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

  @SneakyThrows @NotNull @Override @WebMethod public Project createNewProject(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String dateOfStart,
      @NotNull @WebParam(name = "finish") final String dateOfFinish
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().create(session.getUserId(), projectName, projectDescription, dateOfStart, dateOfFinish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> findAllProject(
      @NotNull @WebParam(name = "session") final Session session
  ) {
    serviceLocator.getSessionService().validate(session);
    if (session.getUserId() == null) throw new AccessForbiddenException();
    return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> findAllProjectByUserId(
      @NotNull @WebParam(name = "session") final Session session
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
  }

  @SneakyThrows @Nullable @Override @WebMethod public Project findProjectById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    return serviceLocator.getProjectService().findOne(session.getUserId(), projectId);
  }

  @SneakyThrows @Override @WebMethod public void removeProjectById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().remove(session.getUserId(), projectId);
  }

  @SneakyThrows @Override @WebMethod public void updateProject(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") final String projectId,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().update(session.getUserId(), projectId, projectName, projectDescription, start, finish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> getSortedProjectList(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "comparator") final String comparatorName
  ) {
    serviceLocator.getSessionService().validate(session);
    final Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService().getComparator(comparatorName);
    return serviceLocator.getProjectService().sortBy(session.getUserId(), comparator);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().findProject(session.getUserId(), partOfTheName);
  }
}
