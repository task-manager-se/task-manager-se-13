package ru.zolov.tm.endpoint;

import java.util.Comparator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskEndpoint;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

  @SneakyThrows @NotNull @Override @WebMethod public Task addNewTask(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "name") final String name,
      @NotNull @WebParam(name = "description") final String description,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  )  {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().create(session.getUserId(), projectId, name, description, start, finish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findAllTasksByUserId(
      @NotNull @WebParam(name = "session") final Session session
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findTasksByProjectId(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
  }

  @SneakyThrows @Nullable @Override @WebMethod public Task findOneTaskById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().findTaskById(taskId);
  }

  @SneakyThrows @Override @WebMethod public void removeOneTaskById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().remove(session.getUserId(), taskId);
  }

  @SneakyThrows @Override @WebMethod public void updateTask(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "taskId") final String taskId,
      @NotNull @WebParam(name = "taskName") final String taskName,
      @NotNull @WebParam(name = "taskDescription") final String taskDescription,
      @NotNull @WebParam(name = "dateOfstart") final String dateOfstart,
      @NotNull @WebParam(name = "dateOffinish") final String dateOffinish
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().update(session.getUserId(), taskId, taskName, taskDescription, dateOfstart, dateOffinish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> getSortedTaskList(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "comparatorName") final String comparatorName
  ) {
    serviceLocator.getSessionService().validate(session);
    final Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService().getComparator(comparatorName);
    return serviceLocator.getTaskService().sortBy(session.getUserId(), projectId, comparator);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findTaskByPartOfTheName(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().findTask(session.getUserId(), partOfTheName);
  }

}
