package ru.zolov.tm.service;

import java.sql.SQLException;
import lombok.Cleanup;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.SessionExpiredException;
import ru.zolov.tm.util.PropertyUtil;
import ru.zolov.tm.util.SignatureUtil;

public class SessionService extends AbstractService<Session> implements ISessionService {

  @NotNull private ServiceLocator serviceLocator;

  public SessionService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @Override public void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException, SessionExpiredException {
    @Cleanup @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
    final String salt = PropertyUtil.SALT;
    final Integer cycle = PropertyUtil.CYCLE;
    if (session == null) throw new AccessForbiddenException();
    if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
    if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
    if (session.getTimestamp() == null) throw new SessionExpiredException();
    @Nullable Session foundedSession = sessionRepository.findOne(session.getId());
    if (foundedSession == null) throw new AccessForbiddenException();
    @NotNull final Session temp = session.clone();
    if (temp == null) throw new AccessForbiddenException();
    @NotNull final String signatureSource = session.getSignature();
    temp.setSignature(null);
    @NotNull final String signatureTarget = SignatureUtil.sign(temp, salt, cycle);
    final boolean check = signatureSource.equals(signatureTarget);
    final boolean live = checkSessionIsAlive(session.getTimestamp());
    if (!check) throw new AccessForbiddenException();
    if (!live) throw new SessionExpiredException();
  }

  @NotNull @Override public Session open(final User user) throws EmptyStringException, SQLException {
    @Cleanup @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
    @NotNull final Session session = new Session();
    @NotNull final String salt = PropertyUtil.SALT;
    @NotNull final Integer cycle = PropertyUtil.CYCLE;
    session.setUserId(user.getId());
    session.setRoleType(user.getRole());
    session.setTimestamp(System.currentTimeMillis());
    session.setSignature(SignatureUtil.sign(session, salt, cycle));
    try {
      sessionRepository.persist(session);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return session;
  }

  @Override public void close(Session session) throws SQLException {
    @Cleanup @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
    try {
      sessionRepository.remove(session.getId());
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  public boolean checkSessionIsAlive(@NotNull final Long sessionTimeStamp) {
    @NotNull final long sessionLiveTimeMs = PropertyUtil.SESSION_LIVETIME;
    @NotNull final long currentTimeMs = System.currentTimeMillis();
    return ((currentTimeMs - sessionTimeStamp) <= sessionLiveTimeMs);
  }
}
