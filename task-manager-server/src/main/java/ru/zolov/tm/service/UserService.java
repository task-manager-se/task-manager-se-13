package ru.zolov.tm.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

  @NotNull private ServiceLocator serviceLocator;

  public UserService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @Override public User login(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, SQLException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    @Nullable User user = null;
    final String passwordHash = HashUtil.md5(password);
    if (passwordHash == null) throw new UserNotFoundException();
    user = userRepository.getExistingUser(login, passwordHash);
    if (user == null) throw new UserNotFoundException();
    return user;
  }

  @NotNull @Override public User userRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserExistException, SQLException, EmptyRepositoryException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    @Nullable String result = userRepository.findByLogin(login);
    if (login.equals(result)) throw new UserExistException();
    @NotNull final User newUser = new User();
    newUser.setLogin(login);
    newUser.setRole(RoleType.USER);
    @Nullable String hashedPassword = HashUtil.md5(password);
    if (hashedPassword == null) throw new EmptyStringException();
    newUser.setPasswordHash(hashedPassword);
    try {
      userRepository.persist(newUser);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return newUser;
  }

  @Override public User adminRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, SQLException, EmptyRepositoryException, UserExistException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    @Nullable String result = userRepository.findByLogin(login);
    if (login.equals(result)) throw new UserExistException();
    @NotNull final User admin = new User();
    admin.setLogin(login);
    admin.setRole(RoleType.ADMIN);
    @NotNull String hashedPassword = HashUtil.md5(password);
    admin.setPasswordHash(hashedPassword);
    try {
      userRepository.persist(admin);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return admin;
  }

  @NotNull public List<User> findAll(@NotNull final Session session) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    List<User> list = new ArrayList<>();
    list = userRepository.findAll();
    return list;
  }

  @Override public void remove(@Nullable final String id) throws EmptyStringException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    try {
      userRepository.remove(id);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  @NotNull @Override public User updateUserPassword(
      @Nullable final String id,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    @Nullable User user = null;
    user = userRepository.findOne(id);
    if (user == null) throw new UserNotFoundException();
    @NotNull String hashedPassword = HashUtil.md5(password);
    user.setPasswordHash(hashedPassword);
    try {
      userRepository.merge(user);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return user;
  }

  @SneakyThrows @NotNull @Override public User updateUserProfile(
      @Nullable String id,
      @Nullable String name
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    @Nullable User user = null;
    user = userRepository.findOne(id);
    if (user == null) throw new UserNotFoundException();
    user.setLogin(name);
    try {
      userRepository.merge(user);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return user;
  }

  @Override public void load(@NotNull Session session, @Nullable List<User> list) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (list == null) throw new EmptyRepositoryException();
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
    if (sqlSession == null) throw new SQLException();
    if (userRepository == null) throw new SQLException();
    for (@Nullable final User user : list) {
      try {
        userRepository.persist(user);
        sqlSession.commit();
      } catch (SQLException e) {
        sqlSession.rollback();
        e.printStackTrace();
      }
    }
  }
}
