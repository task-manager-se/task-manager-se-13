package ru.zolov.tm.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectService extends AbstractService<Project> implements IProjectService {

  @NotNull private ServiceLocator serviceLocator;

  public ProjectService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @SneakyThrows @NotNull public Project create(
      @Nullable final String userId,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (description == null || description.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    @NotNull final Project project = new Project();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    project.setName(name);
    project.setUserId(userId);
    project.setDescription(description);
    project.setDateOfStart(dateFormat.parse(start));
    project.setDateOfFinish(dateFormat.parse(finish));
    try {
      projectRepository.persist(project);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return project;
  }

  @NotNull public Project findOne(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @Nullable Project project = null;
    try {
      if (projectRepository.findOne(id) == null) throw new EmptyRepositoryException();
      project = projectRepository.findOne(id);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return project;
  }

  @NotNull public List<Project> findAllByUserId(
      @Nullable final String userId
  ) throws EmptyStringException, SQLException {
    if (userId == null) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAllByUserId(userId);
    return list;
  }

  @Override @NotNull public List<Project> findAll(Session session) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, SQLException {
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAll();
    return list;
  }

  public void update(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws ParseException, EmptyStringException, EmptyRepositoryException, SQLException {
    if (userId == null || userId.isEmpty()) return;
    if (id == null || id.isEmpty()) return;
    if (name == null || name.isEmpty()) return;
    if (description == null || description.isEmpty()) return;
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull final Project project = new Project();
    @NotNull final Date startDate = dateFormat.parse(start);
    @NotNull final Date finishDate = dateFormat.parse(finish);
    project.setUserId(userId);
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    project.setDateOfStart(startDate);
    project.setDateOfFinish(finishDate);
    try {
      projectRepository.merge(project);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  public void remove(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    try {
      projectRepository.remove(id);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  @NotNull public List<Project> sortBy(
      @Nullable final String userId,
      Comparator<AbstractGoal> comparator
  ) throws EmptyStringException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAllByUserId(userId);
    list.sort(comparator);
    return list;
  }

  @NotNull public List<Project> findProject(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyStringException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull List<Project> defaultlist = new ArrayList<>();
    @NotNull List<Project> list = new ArrayList<>();
    defaultlist = projectRepository.findAllByUserId(userId);
    if (partOfTheName.isEmpty()) return defaultlist;
    list = projectRepository.findProjectByPartOfTheName(userId, partOfTheName);
    return list;
  }

  public void load(@NotNull Session session, @Nullable List<Project> list) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @Nullable @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    if (sqlSession == null) throw new SQLException();
    if (projectRepository == null) throw new SQLException();
    if (list == null) throw new EmptyRepositoryException();
    for (@Nullable final Project project : list) {
      try {
        projectRepository.persist(project);
        sqlSession.commit();
      } catch (SQLException e) {
        sqlSession.rollback();
        e.printStackTrace();
      }
    }
  }
}
