package ru.zolov.tm.service;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITerminalService;
import ru.zolov.tm.comparator.DateCreateComparator;
import ru.zolov.tm.comparator.DateFinishComparator;
import ru.zolov.tm.comparator.DateStartComparator;
import ru.zolov.tm.comparator.StatusComparator;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;


public class TerminalService extends AbstractService<AbstractEntity> implements ITerminalService {

  private static final Map<String, Comparator<AbstractGoal>> comparators = new HashMap<>();
  private static final Comparator<AbstractGoal> dateCreateComparator = new DateCreateComparator();
  private static final Comparator<AbstractGoal> dateStartComparator = new DateStartComparator();
  private static final Comparator<AbstractGoal> dateFinishComparator = new DateFinishComparator();
  private static final Comparator<AbstractGoal> statusComparator = new StatusComparator();

  static {
    comparators.put("date-create", dateCreateComparator);
    comparators.put("date-start", dateStartComparator);
    comparators.put("date-finish", dateFinishComparator);
    comparators.put("status", statusComparator);
  }

  private final Scanner scanner = new Scanner(System.in);

  @Override
  public String nextLine() {
    return scanner.nextLine();
  }

  @Override
  public Integer nextInt() {
    return scanner.nextInt();
  }

  @NotNull
  @Override
  public Comparator<AbstractGoal> getComparator(@Nullable String name) throws EmptyStringException {
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    return comparators.get(name.toLowerCase());
  }

  @Override
  public void performGoal(
      @Nullable final AbstractGoal goal,
      @Nullable final String userName
  ) throws EmptyRepositoryException, EmptyStringException {
    if (goal == null) throw new EmptyRepositoryException();
    if (userName == null || userName.isEmpty()) throw new EmptyStringException();
    final String name = goal.getName();
    final String status = goal.getStatus().getDisplayName();
    final String dateOfCreate = performDate(goal.getDateOfCreate());
    final String dateOfStart = performDate(goal.getDateOfStart());
    final String dateOfFinish = performDate(goal.getDateOfFinish());
    final String id = goal.getId();
    System.out
        .printf("%n Name: %s %n Creator: %s %n Status: %s %n Date of create: %s %n Date of start: %s %n Date of finish: %s %n ID: %s %n", name, userName, status, dateOfCreate, dateOfStart, dateOfFinish, id);
  }

  @Override
  public void performList(
      @NotNull final List<? extends AbstractGoal> list,
      @NotNull final String userName
  ) throws EmptyRepositoryException, EmptyStringException {
    int position = 1;
    for (AbstractGoal goal : list) {
      System.out.print(position + ": ");
      performGoal(goal, userName);
      position++;
    }
  }

  @NotNull
  @Override
  public String performDate(Date date) {
    if (date == null) return "Date has not been set";
    return dateFormat.format(date);
  }
}