package ru.zolov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.entity.Session;

public class DomainService implements IDomainService {

  protected ServiceLocator serviceLocator;

  public DomainService(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @SneakyThrows public void load(@NotNull Session session, @NotNull final Domain domain) {
    serviceLocator.getProjectService().load(session, domain.getProjects());
    serviceLocator.getTaskService().load(session, domain.getTasks());
    serviceLocator.getUserService().load(session, domain.getUsers());

  }

  @SneakyThrows public void save(@NotNull Session session, @NotNull final Domain domain) {
    domain.setProjects(serviceLocator.getProjectService().findAll(session));
    domain.setTasks(serviceLocator.getTaskService().findAll(session));
    domain.setUsers(serviceLocator.getUserService().findAll(session));
  }
}
