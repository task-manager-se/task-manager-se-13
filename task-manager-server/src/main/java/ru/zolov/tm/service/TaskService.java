package ru.zolov.tm.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.Cleanup;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskService extends AbstractService<Task> implements ITaskService {

  @NotNull private ServiceLocator serviceLocator;

  public TaskService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @NotNull public Task create(
      @Nullable final String userId,
      @Nullable final String projectId,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws EmptyStringException, ParseException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (description == null || description.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    Task task = new Task();
    task.setUserId(userId);
    task.setProjectId(projectId);
    task.setName(name);
    task.setDescription(description);
    task.setDateOfStart(dateFormat.parse(start));
    task.setDateOfFinish(dateFormat.parse(finish));
    try {
      taskRepository.persist(task);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
    return task;
  }

  @NotNull public List<Task> findAllByUserId(@Nullable String userId) throws EmptyStringException, SQLException {
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAllByUserId(userId);
    return list;
  }

  @NotNull public List<Task> findAll(@NotNull Session session) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAll();
    return list;
  }

  public List<Task> findTaskByProjectId(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAllByProjectIdByUserId(userId, id);
    return list;
  }

  @Nullable public Task findTaskById(
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @Nullable Task task = taskRepository.findOne(id);
    if (task == null) throw new EmptyRepositoryException();
    return task;
  }

  public void remove(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException {
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    try {
      taskRepository.remove(id);
      sqlSession.commit();
    } catch (SQLException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  public void update(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws EmptyStringException, ParseException, EmptyRepositoryException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @NotNull final Task task = new Task();
    task.setId(id);
    task.setUserId(userId);
    task.setName(name);
    task.setDescription(description);
    @NotNull final Date startDate = dateFormat.parse(start);
    task.setDateOfStart(startDate);
    @NotNull final Date finishDate = dateFormat.parse(finish);
    task.setDateOfFinish(finishDate);
    try {
      taskRepository.merge(task);
      sqlSession.commit();
    } catch (EmptyStringException e) {
      sqlSession.rollback();
      e.printStackTrace();
    }
  }

  @NotNull public List<Task> sortBy(
      @Nullable final String userId,
      @Nullable final String projectId,
      @Nullable Comparator<AbstractGoal> comparator
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    List<Task> list = new ArrayList<>();
    list = taskRepository.findAllByProjectIdByUserId(userId, projectId);
    list.sort(comparator);
    return list;
  }

  @NotNull public List<Task> findTask(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null) throw new EmptyStringException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findTaskByPartOfTheName(userId, partOfTheName);
    return list;
  }

  public void load(@NotNull Session session, @Nullable List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException {
    if (list == null) throw new EmptyRepositoryException();
    if (session.getRoleType() == null) throw new AccessForbiddenException();
    if (session.getRoleType().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull @Cleanup final SqlSession sqlSession = serviceLocator.getSqlSessionFactory().openSession();
    @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    if (sqlSession == null) throw new SQLException();
    if (taskRepository == null) throw new SQLException();
    if (list == null) throw new EmptyRepositoryException();
    for (@Nullable final Task task : list) {
      try {
        taskRepository.persist(task);
        sqlSession.commit();
      } catch (SQLException e) {
        sqlSession.rollback();
        e.printStackTrace();
      }
    }
  }
}
