package ru.zolov.tm.enumerated;

public enum StatusType {
  PLANNED("Planned"),
  INPROGRESS("In progress"),
  DONE("Done");
  private String displayName;

  StatusType(String displayName) {
    this.displayName = displayName;
  }

  public String getDisplayName() {
    return displayName;
  }
}
