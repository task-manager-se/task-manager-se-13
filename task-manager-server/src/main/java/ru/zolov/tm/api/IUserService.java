package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

public interface IUserService {

  @Nullable User login(
      @Nullable String login,
      @Nullable String password
  ) throws EmptyStringException, UserNotFoundException, SQLException, EmptyRepositoryException;

  @NotNull User userRegistration(
      @Nullable String login,
      @Nullable String password
  ) throws EmptyStringException, UserExistException, SQLException, EmptyRepositoryException;

  User adminRegistration(
      @Nullable String login,
      @Nullable String password
  ) throws EmptyStringException, SQLException, EmptyRepositoryException, UserExistException;

  @NotNull List<User> findAll(@NotNull Session session) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;

  void remove(@Nullable String id) throws EmptyStringException, SQLException;

  @NotNull User updateUserPassword(
      @Nullable String id,
      @Nullable String password
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException;

  @NotNull User updateUserProfile(
      @Nullable String id,
      @Nullable String name
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException;

  void load(@NotNull Session session, @Nullable final List<User> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;
}
