package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IUserRepository {

  @Select("SELECT * FROM tmdb_zolov.app_user WHERE login = #{login} AND password_hash = #{passwordHash}") @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "login", column = "login"),
      @Result(property = "passwordHash", column = "password_hash"),
      @Result(property = "role", column = "role")})
  @Nullable User getExistingUser(
      @Param("login") @NotNull final String login,
      @Param("passwordHash") @NotNull final String passwordHash
  ) throws SQLException;

  @Select("SELECT * FROM tmdb_zolov.app_user WHERE login = #{login}")
  @Nullable String findByLogin(@NotNull final String login) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Select("SELECT * FROM tmdb_zolov.app_user;")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "login", column = "login"),
      @Result(property = "passwordHash", column = "password_hash"),
      @Result(property = "role", column = "role")})
  @NotNull List<User> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select("SELECT * FROM tmdb_zolov.app_user WHERE id = #{id}")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "login", column = "login"),
      @Result(property = "passwordHash", column = "password_hash"),
      @Result(property = "role", column = "role")})
  @Nullable User findOne(@NotNull String id) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Insert("INSERT INTO tmdb_zolov.app_user (id, login, password_hash, role) VALUES (#{id}, #{login}, #{passwordHash}, #{role})")
  void persist(@NotNull User entity) throws SQLException, EmptyStringException;

  @Update("UPDATE tmdb_zolov.app_user SET login = #{login} , password_hash = #{passwordHash} , role = #{role} WHERE  id = #{id}")
  void merge(@NotNull User entity) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_user WHERE id=#{id};")
  void remove(@NotNull String id) throws SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_user;")
  void removeAll() throws SQLException;

}
