package ru.zolov.tm.api;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

  @NotNull SqlSessionFactory getSqlSessionFactory();

  @NotNull IProjectService getProjectService();

  @NotNull ITaskService getTaskService();

  @NotNull ITerminalService getTerminalService();

  @NotNull IUserService getUserService();

  @NotNull IDomainService getDomainService();

  @NotNull ISessionService getSessionService();
}
