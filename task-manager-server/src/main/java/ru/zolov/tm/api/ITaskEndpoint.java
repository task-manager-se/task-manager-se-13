package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@WebService
public interface ITaskEndpoint {

  @NotNull @WebMethod Task addNewTask(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId,
      @NotNull @WebParam(name = "name") String name,
      @NotNull @WebParam(name = "description") String description,
      @NotNull @WebParam(name = "start") String start,
      @NotNull @WebParam(name = "finish") String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod List<Task> findAllTasksByUserId(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod List<Task> findTasksByProjectId(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @Nullable @WebMethod Task findOneTaskById(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId,
      @NotNull @WebParam(name = "taskId") String taskId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void removeOneTaskById(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "taskId") String taskId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void updateTask(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "taskId") String taskId,
      @NotNull @WebParam(name = "taskName") String taskName,
      @NotNull @WebParam(name = "taskDescription") String taskDescription,
      @NotNull @WebParam(name = "dateOfStart") String start,
      @NotNull @WebParam(name = "dateOfFinish") String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod List<Task> getSortedTaskList(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId,
      @NotNull @WebParam(name = "comparatorName") String comparatorName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod List<Task> findTaskByPartOfTheName(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "partOfTheName") String partOfTheName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException, SQLException;
}
