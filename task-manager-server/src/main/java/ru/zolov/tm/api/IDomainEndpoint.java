package ru.zolov.tm.api;

import java.io.IOException;
import java.sql.SQLException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.SessionExpiredException;

@WebService
public interface IDomainEndpoint {

  @WebMethod void saveToBin(
      @NotNull @WebParam(name = "session") Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void loadFromBin(
      @NotNull @WebParam(name = "session") Session session
  ) throws IOException, ClassNotFoundException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void saveToJsonJackson(
      @NotNull @WebParam(name = "session") Session session
  ) throws EmptyStringException, EmptyRepositoryException, IOException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void loadFromJsonJackson(
      @NotNull @WebParam(name = "session") Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void loadFromJsonJaxb(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, JAXBException, EmptyStringException, EmptyRepositoryException, SQLException, SessionExpiredException;

  @WebMethod void saveToJsonJaxb(
      @NotNull @WebParam(name = "session") Session session
  ) throws EmptyStringException, EmptyRepositoryException, IOException, JAXBException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void saveToXmlJaxb(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, IOException, JAXBException, SQLException, SessionExpiredException;

  @WebMethod void loadFromXmlJaxb(
      @NotNull @WebParam(name = "session") Session session
  ) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, JAXBException, SQLException, SessionExpiredException;

  @WebMethod void saveToXmlJackson(
      @NotNull @WebParam(name = "session") Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;

  @WebMethod void loadFromXmlJackson(
      @NotNull @WebParam(name = "session") Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, SQLException, SessionExpiredException;
}
