package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectService {

  @NotNull Project create(
      @Nullable String userId,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, ParseException, SQLException;

  @NotNull Project findOne(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> findAllByUserId(@Nullable String userId) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> findAll(Session session) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, SQLException;

  void update(
      @Nullable String userId,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws ParseException, SQLException, EmptyStringException, EmptyRepositoryException;

  void remove(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> sortBy(
      @Nullable String userId,
      @Nullable Comparator<AbstractGoal> comparator
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @NotNull List<Project> findProject(
      @Nullable String userId,
      @Nullable String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException;

  void load(@NotNull Session session, @Nullable List<Project> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;
}
