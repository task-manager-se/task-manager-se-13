package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskService {

  @NotNull Task create(
      @Nullable String userId,
      @Nullable String projectId,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException, SQLException;

  @NotNull List<Task> findAllByUserId(@Nullable String userId) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Task> findAll(
      @NotNull Session session
  ) throws EmptyStringException, EmptyRepositoryException, SQLException, AccessForbiddenException;

  @Nullable List<Task> findTaskByProjectId(
      @Nullable String userId,
      String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Nullable Task findTaskById(
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void remove(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void update(
      @Nullable String userId,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException, SQLException;

  @NotNull List<Task> sortBy(
      @Nullable String userId,
      @Nullable String projectId,
      @Nullable Comparator<AbstractGoal> comparator
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Task> findTask(
      @Nullable String userId,
      @Nullable String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException;

  void load(@NotNull Session session, @Nullable final List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;
}
