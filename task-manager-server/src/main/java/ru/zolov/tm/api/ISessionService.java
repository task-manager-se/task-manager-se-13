package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.SessionExpiredException;

public interface ISessionService {

  @NotNull Session open(User user) throws ParseException, SQLException, EmptyStringException;

  void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException, SQLException, EmptyStringException, EmptyRepositoryException, SessionExpiredException;

  void close(Session session) throws SQLException;
}
