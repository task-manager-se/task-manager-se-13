package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@WebService
public interface IProjectEndpoint {

  @NotNull @WebMethod Project createNewProject(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectName") String projectName,
      @NotNull @WebParam(name = "projectDescription") String projectDescription,
      @NotNull @WebParam(name = "dateOfStart") String dateOfStart,
      @NotNull @WebParam(name = "dateOfFinish") String dateOfFinish
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, ParseException, SQLException, EmptyRepositoryException;

  @NotNull @WebMethod List<Project> findAllProject(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod List<Project> findAllProjectByUserId(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @Nullable @WebMethod Project findProjectById(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void removeProjectById(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void updateProject(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "projectId") String projectId,
      @NotNull @WebParam(name = "projectName") String projectName,
      @NotNull @WebParam(name = "projectDescription") String projectDescription,
      @NotNull @WebParam(name = "dateOfStart") String start,
      @NotNull @WebParam(name = "dateOfFinish") String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyRepositoryException, SQLException, EmptyStringException;

  @NotNull @WebMethod List<Project> getSortedProjectList(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "comparatorName") String comparatorName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException, SQLException;

  @NotNull @WebMethod List<Project> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "partOfTheName") String partOfTheName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException, SQLException;
}
