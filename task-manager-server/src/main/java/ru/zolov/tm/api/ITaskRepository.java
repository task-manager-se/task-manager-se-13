package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskRepository {

  @Select("SELECT * FROM tmdb_zolov.app_task WHERE user_id = #{userId}")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"),
      @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"),
      @Result(property = "name", column = "name"),
      @Result(property = "description", column = "description"),
      @Result(property = "status", column = "status"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "projectId", column = "project_id")
  })
  @NotNull List<Task> findAllByUserId(@NotNull String userId) throws SQLException, EmptyStringException;

  @Select("DELETE * FROM tmdb_zolov.app_task WHERE user_id = #{userId}")
  void removeAllByUserId(@NotNull String userId) throws SQLException;

  @Select("SELECT * FROM tmdb_zolov.app_task WHERE project_id = #{projectId} AND user_id = #{userId}"
  )
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "name", column = "name"),
      @Result(property = "createdData", column = "date_create"),
      @Result(property = "endDate", column = "date_end"),
      @Result(property = "startDate", column = "date_start"),
      @Result(property = "description", column = "description"),
      @Result(property = "status", column = "status"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "idProject", column = "project_id")
  })
  @NotNull List<Task> findAllByProjectIdByUserId(
      @Param("userId") @NotNull String userId,
      @Param("projectId") @NotNull String projectId
  ) throws SQLException, EmptyRepositoryException, EmptyStringException;

  @Delete(
      "DELETE FROM tmdb_zolov.app_task WHERE project_id = #{projectId} AND user_id = #{userId}"
  )
  void removeAllTaskByProjectId(
      @Param("userId") @NotNull String userId,
      @Param("projectId") @NotNull String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select("SELECT * FROM tmdb_zolov.app_task WHERE project_id = #{projectID}")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"),
      @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"),
      @Result(property = "name", column = "name"),
      @Result(property = "description", column = "description"),
      @Result(property = "status", column = "status"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "projectId", column = "project_id")
  })
  @NotNull List<Task> findAllByProjId(
      @NotNull String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select(
      "SELECT * FROM tmdb_zolov.app_project WHERE user_id = #{userId} AND (name LIKE CONCAT('%' #{partOfTheName} '%' OR description LIKE '%  #{partOfTheName} '%');")
  @Results(
      value = {
          @Result(property = "id", column = "id"),
          @Result(property = "dateOfCreate", column = "date_create"),
          @Result(property = "dateOfStart", column = "date_start"),
          @Result(property = "dateOfFinish", column = "date_finish"),
          @Result(property = "description", column = "description"),
          @Result(property = "name", column = "name"),
          @Result(property = "status", column = "status"),
          @Result(property = "userId", column = "user_id")
      })
  @NotNull List<Task> findTaskByPartOfTheName(
      @Param("userId") @NotNull String userId,
      @Param("partOfTheName") @NotNull String partOfTheName
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select("SELECT * FROM tmdb_zolov.app_task;")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"),
      @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"),
      @Result(property = "name", column = "name"),
      @Result(property = "description", column = "description"),
      @Result(property = "status", column = "status"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "projectId", column = "project_id")
  })
  @NotNull List<Task> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select("SELECT * FROM tmdb_zolov.app_task WHERE id = #{id};") @Results(value = {@Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"), @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"), @Result(property = "name", column = "name"),
      @Result(property = "description", column = "description"), @Result(property = "status", column = "status"),
      @Result(property = "userId", column = "user_id"), @Result(property = "projectId",
      column = "project_id")
  })
  @Nullable Task findOne(@NotNull String id) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Insert("INSERT INTO tmdb_zolov.app_task (id, date_create, date_start, date_finish, description, name, status, user_id, project_id) "
      + "VALUES (#{id}, #{dateOfCreate}, #{dateOfStart}, #{dateOfFinish}, #{description}, #{name}, #{status}, #{userId}, #{projectId});")
  void persist(@NotNull Task entity) throws SQLException, EmptyStringException;

  @Update("UPDATE tmdb_zolov.app_task SET date_create = #{dateOfCreate}, date_start = #{dateOfStart}, date_finish = #{dateOfFinish}, "
      + "description = #{description}, name = #{name}, status = #{status}, user_id = #{userId}, project_id = #{projectId} WHERE id = #{id};")
  void merge(@NotNull Task entity) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_task WHERE id = #{id};")
  void remove(@NotNull String id) throws SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_task;")
  void removeAll() throws SQLException;
}
