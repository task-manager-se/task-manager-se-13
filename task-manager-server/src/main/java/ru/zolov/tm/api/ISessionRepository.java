package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ISessionRepository {

  @Select(
      "SELECT * FROM tmdb_zolov.app_session"
  )
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "signature", column = "signature"),
      @Result(property = "timestamp", column = "timestamp")
  })
  @NotNull List<Session> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select(
      "SELECT * FROM tmdb_zolov.app_session WHERE id = #{id}")
  @Results(value = {
      @Result(property = "id", column = "id"),
      @Result(property = "userId", column = "user_id"),
      @Result(property = "signature", column = "signature"),
      @Result(property = "timestamp", column = "timestamp")
  })
  @Nullable Session findOne(@NotNull String id) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Insert("INSERT INTO tmdb_zolov.app_session (id, signature, timestamp, user_id) VALUES (#{id}, #{signature}, #{timestamp}, #{userId})"
  )
  void persist(@NotNull Session entity) throws SQLException, EmptyStringException;

  @Update("UPDATE tmdb_zolov.app_session SET signature = #{signature}, timestamp = #{timestamp}, user_id = #{userId} WHERE id = #{id}"
  )
  void merge(@NotNull Session entity) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Session> findAllByUserId(@NotNull String userId) throws SQLException, EmptyStringException;

  @Delete("DELETE FROM tmdb_zolov.app_session WHERE id = #{id}"
  )
  void remove(@NotNull String id) throws SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_session"
  )
  void removeAll() throws SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_session WHERE user_id = #{userId}")
  void removeAllByUserId(@NotNull String userId) throws SQLException;
}
