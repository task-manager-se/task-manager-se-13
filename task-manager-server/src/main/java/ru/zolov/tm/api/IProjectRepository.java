package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectRepository {

  @Select("Select * FROM tmdb_zolov.app_project WHERE id=#{userId} AND id = #{id}") @Results(value = {
      @Result(property = "id", column = "id"), @Result(property = "dateOfCreate", column = "date_create"),
      @Result(property = "dateOfStart", column = "date_start"), @Result(property = "dateOfFinish", column = "date_finish"),
      @Result(property = "description", column = "description"), @Result(property = "name", column = "name"),
      @Result(property = "status", column = "status"), @Result(property = "userId", column = "user_id")
  })
  @Nullable Project findOneByUserId(
      @Param("userId") @NotNull String userId,
      @Param("id") @NotNull String id
  ) throws SQLException, EmptyStringException;

  @Select("Select * FROM tmdb_zolov.app_project WHERE id=#{userId}") @Results(value = {@Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"), @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"), @Result(property = "description", column = "description"),
      @Result(property = "name", column = "name"), @Result(property = "status", column = "status"), @Result(property = "userId",
      column = "user_id")
  })
  @NotNull List<Project> findAllByUserId(@NotNull String userId) throws SQLException, EmptyStringException;

  @Delete("DELETE FROM tmdb_zolov.app_project WHERE id=#{userId}") void removeAllByUserId(@NotNull String userId) throws SQLException;

  @Select("Select * FROM tmdb_zolov.app_project") @Results(value = {@Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"), @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"), @Result(property = "description", column = "description"),
      @Result(property = "name", column = "name"), @Result(property = "status", column = "status"), @Result(property = "userId",
      column = "user_id")
  })
  @NotNull List<Project> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Select("SELECT * FROM tmdb_zolov.app_project WHERE id = #{id}") @Results(value = {@Result(property = "id", column = "id"),
      @Result(property = "dateOfCreate", column = "date_create"), @Result(property = "dateOfStart", column = "date_start"),
      @Result(property = "dateOfFinish", column = "date_finish"), @Result(property = "description", column = "description"),
      @Result(property = "name", column = "name"), @Result(property = "status", column = "status"), @Result(property = "userId",
      column = "user_id")
  })
  @Nullable Project findOne(@NotNull String id) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Insert("INSERT INTO tmdb_zolov.app_project " + "(id, date_create, date_start, date_finish, description, name, status, user_id) "
      + "VALUES (#{id}, #{dateOfCreate}, #{dateOfStart}, #{dateOfFinish}, #{description}, #{name}, #{status}, #{userId});")
  void persist(@NotNull Project entity) throws SQLException, EmptyStringException;

  @Update("UPDATE tmdb_zolov.app_project SET date_create = #{dateOfCreate}, date_start = #{dateOfStart}, date_finish = #{dateOfFinish},"
      + " description = #{description}, name = #{name}, status = #{status}, user_id = #{userId} WHERE id = #{id};")
  void merge(@NotNull Project entity) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_project WHERE id=#{id}")
  void remove(@NotNull String id) throws SQLException;

  @Delete("DELETE FROM tmdb_zolov.app_project")
  void removeAll() throws SQLException;

  @Select(
      "SELECT * FROM tmdb_zolov.app_project WHERE user_id =  #{userId} AND (name LIKE CONCAT('%' #{partOfTheName} '%' OR description LIKE '%  #{partOfTheName} '%');") @Results(
      value = {@Result(property = "id", column = "id"), @Result(property = "dateOfCreate", column = "date_create"),
          @Result(property = "dateOfStart", column = "date_start"), @Result(property = "dateOfFinish", column = "date_finish"),
          @Result(property = "description", column = "description"), @Result(property = "name", column = "name"),
          @Result(property = "status", column = "status"),
          @Result(property = "userId", column = "user_id")
      })
  @NotNull List<Project> findProjectByPartOfTheName(
      @Param("userId") @NotNull String userId,
      @Param("partOfTheName") @NotNull String partOfTheName
  ) throws SQLException, EmptyStringException;
}
