package ru.zolov.tm.util;

import javax.sql.DataSource;
import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.IUserRepository;

public class SqlSessionFactoryUtil {

  private SqlSessionFactoryUtil() {
  }

  @SneakyThrows @NotNull public static SqlSessionFactory getSqlSessionFactory() {
    @NotNull final String user = PropertyUtil.MYSQL_USER;
    @NotNull final String password = PropertyUtil.MYSQL_PASSWORD;
    @NotNull final String url = PropertyUtil.MYSQL_URL;
    @NotNull final String driver = PropertyUtil.MYSQL_DRIVER;

    final DataSource dataSource = new PooledDataSource(driver, url, user, password);

    final TransactionFactory transactionFactory = new JdbcTransactionFactory();

    final Environment environment = new Environment("development", transactionFactory, dataSource);

    Configuration configuration = new Configuration(environment);
    configuration.addMapper(IProjectRepository.class);
    configuration.addMapper(ITaskRepository.class);
    configuration.addMapper(IUserRepository.class);
    configuration.addMapper(ISessionRepository.class);

    return new SqlSessionFactoryBuilder().build(configuration);
  }
}

