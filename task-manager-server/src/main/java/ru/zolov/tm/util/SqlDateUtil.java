package ru.zolov.tm.util;

import java.sql.Date;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.exception.EmptyStringException;

public class SqlDateUtil {

  private SqlDateUtil() {}

  public static Date convertToSqlDate(@Nullable java.util.Date date) throws EmptyStringException {
    Date sqlDate = null;
    if (date == null) throw new EmptyStringException();
    sqlDate = new Date(date.getTime());
    return sqlDate;
  }

  public static java.util.Date convertToDate(@Nullable Date sqlDate) throws EmptyStringException {
    java.util.Date date = null;
    if (sqlDate == null) throw new EmptyStringException();
    date = new java.util.Date(sqlDate.getTime());
    return date;
  }
}
