package ru.zolov.tm.util;

import java.util.Set;
import javax.xml.ws.Endpoint;
import ru.zolov.tm.endpoint.AbstractEndpoint;

public class PublisherUtil {

  public static final String URL = "http://127.0.0.1:8080/";
  public static final String POSTFIX = "?wsdl";

  private PublisherUtil() {}

  public static void publish(Set<AbstractEndpoint> endpoints) {
    for (AbstractEndpoint endpoint : endpoints) {
      publish(endpoint);
    }
  }

  public static void publish(AbstractEndpoint endpoint) {
    String endpointName = endpoint.getClass().getSimpleName();
    String link = URL + endpointName + POSTFIX;
    System.out.println("[" + endpointName + "]" + " published at " + link);
    Endpoint.publish(link, endpoint);
  }
}
