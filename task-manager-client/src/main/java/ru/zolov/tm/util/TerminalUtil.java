package ru.zolov.tm.util;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Scanner;


public class TerminalUtil {


  private static final Scanner scanner = new Scanner(System.in);
  public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);

  public static String nextLine() {
    return scanner.nextLine();
  }

  public static Integer nextInt() {
    return scanner.nextInt();
  }

}
