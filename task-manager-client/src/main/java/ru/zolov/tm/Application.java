package ru.zolov.tm;

import ru.zolov.tm.loader.Bootstrap;

public class Application {

  public static void main(String[] args) throws Exception {

    Bootstrap bootstrap = new Bootstrap();
    bootstrap.init();
  }
}
