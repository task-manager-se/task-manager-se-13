package ru.zolov.tm.command.user;

import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.UserNotFoundException_Exception;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public class UserPasswordUpdateCommand extends AbstractCommand {

  private final String name = "user-password-upd";
  private final String description = "Update password";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, UserNotFoundException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception, SQLException_Exception {
    Session session = bootstrap.getCurrentSession();
    System.out.println("Enter new password: ");
    final String password = TerminalUtil.nextLine();
    bootstrap.getUserEndpoint().updateUserPassword(session, password);
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
