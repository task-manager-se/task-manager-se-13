package ru.zolov.tm.command.system;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.Project;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.Task;
import ru.zolov.tm.util.TerminalUtil;

public class SearchCommand extends AboutCommand {

  private final String name = "search";
  private final String description = "Search project and task by part of the name";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    System.out.println("Enter part of the project or task name: ");
    @NotNull final String partOfTheName = TerminalUtil.nextLine().trim();
    @NotNull final List<Project> projectList = bootstrap.getProjectEndpoint().findProjectByPartOfTheName(session, partOfTheName);
    @NotNull final List<Task> taskList = bootstrap.getTaskEndpoint().findTaskByPartOfTheName(session, partOfTheName);
    if (partOfTheName.isEmpty()) System.out.println("The search criteria you entered is empty.");
    for (Project project : projectList) {
      System.out.println(
          "%n Project: " + project.getName() + "%n Project ID " + project.getId() + "%n Project description:" + project.getDescription());
    }
    for (Task task : taskList) {
      System.out
          .println("%n Task: " + task.getName() + "%n Project ID " + task.getId() + "%n Project description:" + task.getDescription());
    }
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
