package ru.zolov.tm.command.project;

import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.ParseException_Exception;
import ru.zolov.tm.api.Project;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public final class ProjectEditCommand extends AbstractCommand {

  private final String name = "project-edit";
  private final String description = "Edit project";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws ParseException, EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception, ParseException_Exception, SQLException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    final List<Project> listOfProjects = bootstrap.getProjectEndpoint().findAllProject(session);
    for (Project project : listOfProjects) {

      System.out.printf(
          "%n Project: %s " + "%n Project ID: %s " + "%n Project description: %s " + "%n Status: %s " + "%n Date of create: %s "
              + "%n Date of start: %s " + "%n Date of finish: %s", project.getName(), project.getId(), project.getDescription(), project
              .getStatus(), project.getDateOfCreate(), project.getDateOfStart(), project.getDateOfFinish());
    }

    System.out.println("Enter project id: ");
    final String projectId = TerminalUtil.nextLine();
    System.out.println("Enter project name: ");
    final String projectName = TerminalUtil.nextLine();
    System.out.println("Enter new description: ");
    final String projectDescription = TerminalUtil.nextLine();
    System.out.println("Enter date of project start in format DD.MM.YYYY: ");
    final String start = TerminalUtil.nextLine();
    System.out.println("Enter date of project finish in format DD.MM.YYYY: ");
    final String finish = TerminalUtil.nextLine();
    bootstrap.getProjectEndpoint().updateProject(session, projectId, projectName, projectDescription, start, finish);
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
