package ru.zolov.tm.command.task;

import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.ParseException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.Task;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public final class TaskEditCommand extends AbstractCommand {

  private final String name = "task-edit";
  private final String description = "Edit task";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws ParseException, EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception, ParseException_Exception, SQLException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    @NotNull final List<Task> listOfTasks = bootstrap.getTaskEndpoint().findAllTasksByUserId(session);
    for (Task task : listOfTasks) {
      System.out
          .println("%n Task: " + task.getName() + "%n Project ID " + task.getId() + "%n Project description:" + task.getDescription());
    }
    System.out.print("Enter task id: ");
    final String id = TerminalUtil.nextLine();
    System.out.print("Enter new task name: ");
    final String name = TerminalUtil.nextLine();
    System.out.print("Enter new description: ");
    final String description = TerminalUtil.nextLine();
    System.out.println("Enter date of project start in format DD.MM.YYYY: ");
    final String start = TerminalUtil.nextLine();
    System.out.println("Enter date of project finish in format DD.MM.YYYY: ");
    final String finish = TerminalUtil.nextLine();
    bootstrap.getTaskEndpoint().updateTask(session, id, name, description, start, finish);
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
