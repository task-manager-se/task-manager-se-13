package ru.zolov.tm.command.data.json;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.IOException_Exception;
import ru.zolov.tm.api.JAXBException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.SessionExpiredException_Exception;
import ru.zolov.tm.command.AbstractCommand;

public class DataJsonJaxBSaveCommand extends AbstractCommand {

  private final String name = "datasave-json-jaxb";
  private final String description = "Save data to json file (jaxb)";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return true;
  }

  @Override public void execute() throws IOException, JAXBException, AccessForbiddenException_Exception, CloneNotSupportedException_Exception, JAXBException_Exception, EmptyStringException_Exception, IOException_Exception, EmptyRepositoryException_Exception, SQLException_Exception, SessionExpiredException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    final boolean check = (session == null || session.getRoleType() != RoleType.ADMIN);
    if (check) throw new AccessForbiddenException_Exception();
    bootstrap.getDomainEndpoint().saveToJsonJaxb(session);
    System.out.println("Data saved!");
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN};
  }
}
